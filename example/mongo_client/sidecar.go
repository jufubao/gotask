package main

import (
	"context"
	"log"
	"os"
	"time"

	"github.com/hyperf/gotask/v2/pkg/gotask"
	"github.com/hyperf/gotask/v2/pkg/mongo_client"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	mongoConfig := mongo_client.LoadConfig()
	if len(mongoConfig.Uri) == 0 {
		mongoConfig.Uri = os.Getenv("MONGODB_URI")
	}
	if mongoConfig.ConnectTimeout == 0 {
		t, _ := time.ParseDuration(os.Getenv("MONGODB_CONNECT_TIMEOUT"))
		mongoConfig.ConnectTimeout = t
	}
	if mongoConfig.ReadWriteTimeout == 0 {
		t, _ := time.ParseDuration(os.Getenv("MONGODB_READ_WRITE_TIMEOUT"))
		mongoConfig.ReadWriteTimeout = t
	}
	ctx, cancel := context.WithTimeout(context.Background(), mongoConfig.ConnectTimeout)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoConfig.Uri))
	if err != nil {
		log.Fatalln(err)
	}

	if err := gotask.Register(mongo_client.NewMongoProxyWithTimeout(client, mongoConfig.ReadWriteTimeout)); err != nil {
		log.Fatalln(err)
	}

	if err := gotask.Run(); err != nil {
		log.Fatalln(err)
	}
}
